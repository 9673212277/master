@extends('layouts.app')

@section('content')
<div class="w-4/5 m-auto text-left">
    <div class="py-15">
        <h1 class="text-6xl">
            {{ $post->title }}
        </h1>
    </div>
</div>

<div class="w-4/5 m-auto pt-20">
    <span class="text-gray-500">
        By <span class="font-bold italic text-gray-800">{{ $post->user->name }}</span>, Created on {{ date('jS M Y', strtotime($post->updated_at)) }}
    </span>

    <p class="text-xl text-gray-700 pt-8 pb-10 leading-8 font-light">
        {{ $post->description }}
    </p>
</div>
@if ($errors->any())
    <div class="w-4/5 m-auto">
        <ul>
            @foreach ($errors->all() as $error)
                <li class="w-1/5 mb-4 text-gray-50 bg-red-700 rounded-2xl py-4">
                    {{ $error }}
                </li>
            @endforeach
        </ul>
    </div>
@endif

<div>
<span class="sm:grid grid-cols-2 gap-20 w-4/5 mx-auto py-15 border-b border-gray-200">
               <span class="font-bold italic text-gray-800">Comments
            </span>
</div>
@foreach ($comment as $cmd)
    <div class="sm:grid grid-cols-2 gap-20 w-4/5 mx-auto py-15 border-b border-gray-200">
        <!-- <div>
            <img src="{{ asset('images/' . $post->image_path) }}" alt="">
        </div> -->
        <div>
            <h2 class="text-gray-500 font-bold text-xl pb-4">
                {{ $cmd->user->name }}
            </h2>

            <p class="text-xl text-gray-700 pt-8 leading-8 font-light">
                {{ $cmd->msg }}
            </p>

        
        </div>
    </div>    
@endforeach


<div class="w-4/5 m-auto pt-20">
    <form 
        action="/comment"
        method="POST"
        enctype="multipart/form-data">
        @csrf

        <input 
            type="hidden"
            name="post_id"
            value={{$post->id}}
            placeholder="Title...">
        <textarea 
            name="msg"
            placeholder="Comments"
            class="py-20 bg-transparent block border-b-2 w-full h-60 text-xl outline-none"></textarea>            
        <button    
            type="submit"
            class="uppercase mt-15 bg-blue-500 text-gray-100 text-lg font-extrabold py-4 px-8 rounded-3xl">
            Submit
        </button>
    </form>
</div>

@endsection 